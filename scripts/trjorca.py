#!/usr/bin/env python
# coding: utf-8

import os
import shutil
import argparse
from pyorca import OrcaOutfile, OrcaScanCalculation

def get_options():
    """ get options from command lines """
    
    def check_geo(value):
        """ Check if geo is a positive integer or 'all' or 'opt' """
        try:
            i = int(value)
            if i < 0 and i:
                raise argparse.ArgumentTypeError(
                    f"geo is '{value}' and must be a positive integer,'all' or 'opt'")
            return i
        except ValueError:
            if value in {"all", "opt", "last"}:
                return value
            else:
                raise argparse.ArgumentTypeError(
                    f"geo is '{value}' and must be a positive integer,'all' or 'opt'")

    parser = argparse.ArgumentParser(
        description=("Export one geometry or a set of geometries from a current"
                     " ORCA run and save it as a trajectory file."))
    parser.add_argument("-t", "--to",
                        help="output file",
                        metavar="FILE",
                        dest="geofile",
                        default="geo.xyz")
    parser.add_argument("-s", "--scan",
                        help="Calculation is a scan calculation",
                        action="store_true",
                        dest="scan",
                        default=False)
    parser.add_argument("-g", "--geo",
                       help=("Geometry to export from the calculation. "
                             "Default is 'last'.\nGive an"
                             " integer to export a given geometry. In case of"
                             " a scan calculation, it export the gth optimized "
                             " geometry of the scan calculation. If 'all',"
                             " all geometries are exported as a xyz trajectory"
                             " file. If 'opt', in case of a relaxed scan"
                             " calculation, it exports the optimized geometry"
                             " of each step. If 'last' the last geometry is"
                             " exported."),
                       metavar="GEO",
                       default="last",
                       type=check_geo)           
    parser.add_argument("outfile",
                        help="ORCA standard output file to be read",
                        metavar="OUTFILE",
                        type=str)

    return parser
    
    
def make_trj(outfile, geofile, scan, geo):
    """ Read the outfile and export geometries
    
    Args:
        outfile (str): Path to the ORCA output file to be read
        geofile (str): Path to the xyz output file
        scan (bool): If True, a scan calculation is considered
        geo (int, str): index of the geometry 'all', 'last' or 'opt'
    """
    
#    print(outfile, geofile, scan, geo)
    
    # save file if it exits
    if os.path.exists(geofile):
        shutil.copy(geofile, geofile + ".bak")
    
    if scan:
        out = OrcaScanCalculation(outfile)
    else:
        if geo == "opt":
            print("WARNINGS: you ask for geo='opt' but this is not" 
                  " a scan calculations. All geometries will be"
                  " exported. Use -s to consider scan.")
        out = OrcaOutfile(outfile)
        
    try:
        # case geo is an integer
        igeo = int(geo - 1)
        mol = out.structures[igeo]
        print(f"{'geometry':15s}: {igeo}")
        with open(geofile, "w", encoding="utf-8") as fg:
            fg.write(mol.to("xyz"))
    except TypeError:
        with open(geofile, "w", encoding="utf-8") as fg:
            if scan:
                if geo =="all":
                    print("all structures in scan")
                    structures = out.all_structures
                    print(f"{'trajectory':15s}: {len(structures)} geometries")
                elif geo == "last":
                    print("last structure in scan")
                    structures = [out.all_structures[-1]]
                    print(f"{'geometry':15s}: {len(out.all_structures)} (last)")
                elif geo == "opt":
                    print("opt structures in scan")
                    structures = out.structures
                    print(f"{'trajectory':15s}: {len(structures)} geometries")
            else:
                if geo == "last":
                    print("last non-scan geometry")
                    structures = [out.structures[-1]]
                    print(f"{'geometry':15s}: {len(out.structures)} (last)")
                else:
                    print("all non-scan geometry")
                    structures = out.structures
                    print(f"{'trajectory':15s}: {len(structures)} geometries")
            
            # export structures
            for mol in structures:
                fg.write(mol.to("xyz") + "\n")
        

if __name__ == "__main__":
    
    # manage options
    args = vars(get_options().parse_args())
    for key, val in args.items():
        print(f"{key:15s}: {val}")
    make_trj(**args)

    