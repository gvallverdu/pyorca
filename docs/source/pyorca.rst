======
pyorca
======

.. automodule:: pyorca
    :members:

.. contents:: Contents
    :depth: 3

ORCA input file
================

.. autoclass:: pyorca.OrcaInput
    :members:


ORCA output files
=================

ORCA standard output
--------------------

.. autoclass:: pyorca.OrcaOutfile
    :members:

Hessian file
------------

.. autoclass:: pyorca.OrcaHessian
    :members:

VPT2 file
---------

.. autoclass:: pyorca.OrcaVPT2
    :members:

EnGrad file
-----------

.. autoclass:: pyorca.OrcaEnGradfile
    :members:

All files of a run
------------------

.. autoclass:: pyorca.OrcaRun
    :members:

Scan calculations
-----------------

.. autoclass:: pyorca.OrcaScanCalculation
    :members: