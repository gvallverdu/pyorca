==============
pyorca scripts
==============

The ``scripts/`` folder contains scripts to extract data from 
calculations usint the `pyorca` module.

trjorca
=======

This script aims to extract a specific geometry from the output file
or to create a trajectory file by concatenating several geometries. The
output format is xyz.

The script works in cases of scan calculations (relaxed or not) and 
for non terminated calculations.

Usage
-----

.. argparse::
   :filename: ../scripts/trjorca.py
   :func: get_options
   :prog: trjorca