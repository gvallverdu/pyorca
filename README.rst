======
PyORCA
======

.. image:: ../../pyorca_small.png
    :align: center
    :width: 300

`ORCA <https://orcaforum.kofo.mpg.de/app.php/portal>`_ 
is an *ab initio*, DFT and semiempirical SCF-MO package to implement
quantum chemistry calculations.
The PyORCA module provides a set of python classes in order to produce
the ORCA input file or to extract data from several ORCA output files.
The module uses the `Molecule <https://pymatgen.org/pymatgen.core.structure.html#pymatgen.core.structure.Molecule>`_ 
class of `pymatgen <https://pymatgen.org>`_ to represent the geometries
available in input or output files.

Currently, the following files are supported: input file, standard output
file, hessian file (.hess), VPT2 file (.vpt2) and energy and gradient
file (.engrad).

.. toctree::
   :maxdepth: 2

   pyorca
   scripts

Installation
============

Before installing pyorca it is recommanded to create a virtual environment 
using conda or virtuelenv. Then, using pip directly from gitlab, run

::

    python -m pip install git+https://gitlab.com/gvallverdu/pyorca.git

Alternatively, you can first clone the pychemcurv repository and then
install the package using setuptools:

:: 

    git clone https://gitlab.com/gvallverdu/pyorca.git
    cd pyorca/
    python -m pip install .

In order to install in developper mode, first create an environment
clone the repository and then install with the `-e` option

::
    
    git clone https://gitlab.com/gvallverdu/pyorca.git
    cd pyorca/
    python -m pip install -e .[dev]

the dev extras will install the requirements plus autopep8, pytest, 
pylint, jupyter and sphinx.

Quick start
===========

When pyorca is installed you can used it, for example, to read the
standard output of on ORCA calculation:

.. code-block:: python

    # import the OrcaOutfile class
    >>> from pyorca import OrcaOutfile

    # read in a file
    >>> out = OrcaOutfile("./pyorca/test_files/methanamine.out")

    # calcul termination
    >>> out.normal_termination
    True

    >>> out.optimization_converged
    True

    >>> out.final_energy
    -95.835494205734

    # last structure
    >>> out.final_structure
    Molecule Summary
    Site: C (-0.0007, 0.0037, 0.0000)
    Site: N (-0.0155, -1.4589, 0.0000)
    Site: H (-0.5047, -1.8129, 0.8136)
    Site: H (-0.5047, -1.8129, -0.8136)
    Site: H (0.5425, 0.3579, -0.8786)
    Site: H (-0.9897, 0.4831, 0.0000)
    Site: H (0.5425, 0.3579, 0.8786)

    >>> print(out.final_structure.to("xyz"))
    7
    H5 C1 N1
    C -0.000660 0.003685 0.000000
    N -0.015473 -1.458927 0.000000
    H -0.504743 -1.812935 0.813572
    H -0.504743 -1.812935 -0.813572
    H 0.542472 0.357935 -0.878576
    H -0.989673 0.483074 0.000000
    H 0.542472 0.357935 0.878576

    >>> out.final_structure.cart_coords
    array([[-6.600000e-04,  3.685000e-03,  0.000000e+00],
           [-1.547300e-02, -1.458927e+00,  0.000000e+00],
           [-5.047430e-01, -1.812935e+00,  8.135720e-01],
           [-5.047430e-01, -1.812935e+00, -8.135720e-01],
           [ 5.424720e-01,  3.579350e-01, -8.785760e-01],
           [-9.896730e-01,  4.830740e-01,  0.000000e+00],
           [ 5.424720e-01,  3.579350e-01,  8.785760e-01]])

    # properties
    >>> out.frequencies
    [309.75, 831.61, 975.09, 1056.02, 1167.96, 1345.44, 1457.88, 1494.31, 1515.74, 1664.14, 2961.75, 3049.99, 3086.23, 3482.84, 3558.01]

    >>> out.dipole
    1.34371

    >>> out.dipole_moment
    array([-0.51245,  0.12984, -0.     ])

    >>> out.bond_orders
    {(0, 1): 0.9973, (0, 4): 0.9771, (0, 5): 0.9705, (0, 6): 0.9771, (1, 2): 0.9445, (1, 3): 0.9445}

    # thermochemistry
    >>> out.thermochemistry
    True

    # gibbs free enthalpy (eV, kcal.mol-1)
    >>> out.thermo_data["final gibbs free enthalpy"]
    (-95.79368866, -60111.44967419227)

    >>> out.gibbs_free_enthalpy
    -60111.44967419227

    # corresponding input file
    >>> out.orca_input.input_parameters
    ['pal4', 'b3lyp', 'tightscf', 'def2-tzvp', 'opt', 'tightopt', 'numfreq', 'chelpg']



Licence and contact
===================

This package was developped at the `Université de Pau et des Pays de l'Adour
(UPPA) <http://www.univ-pau.fr>`_ in the `Institut des Sciences Analytiques et
de Physico-Chimie pour l'Environement et les Matériaux (IPREM)
<http://iprem.univ-pau.fr/>`_ and is distributed under the 
`MIT licence <https://opensource.org/licenses/MIT>`_.

**Authors**

* Hugo Santos-Silva
* Germain Salvato Vallverdu: `germain.vallverdu@univ-pau.fr <germain.vallverdu@univ-pau.fr>`_

