# coding: utf-8

import setuptools

__author__ = "Hugo Santos-Silva, Germain Salvato Vallverdu"
__email__ = "germain.vallverdu@univ-pau.fr"
__copyright__ = "Copyright 2016, UPPA-CNRS"
__version__ = "2021.04.14"

with open("README.rst", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pyorca",
    version=__version__,
    author=__author__,
    author_email=__email__,
    url="https://gitlab.com/gvallverdu/pyorca",

    # A short description
    description=("Python classes to manage input generation or analysis "
                 "of ORCA output files."),

    # long description
    long_description=long_description,
    long_description_content_type="text/x-rst",

    # requirements
    install_requires=[
        "numpy", "pandas", "pymatgen",
    ],
    # extra requirements
    extras_require={
        "dev": ["autopep8", "pytest", "pylint", "jupyter", "sphinx", 
                "sphinx-argparse"],
    },

    # find_packages()
    packages=setuptools.find_packages(exclude=['tests*']),

    # 
    classifiers=[
        "Programming Language :: Python :: 3",
        "Development Status :: 4 - Beta",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering :: Chemistry",
    ],
    python_requires='>=3.8',

    # entry points
    entry_points={
        "console_scripts": [
            "trjorca = "
        ]
    }
)
