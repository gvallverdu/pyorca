# coding: utf-8

"""
This module implements classes that represents input and output files 
of the ORCA ab initio, DFT and semiempirical SCF-MO package.
"""

__author__ = "Hugo Santos-Silva, Germain Salvato Vallverdu"
__email__ = "germain.vallverdu@univ-pau.fr"
__copyright__ = "Copyright 2016, UPPA-CNRS"
__version__ = "2021.04.14"

# import core classes from pyorca
from .pyorca import *

# import convenient objects from pymatgen
from pymatgen.electronic_structure.core import Spin
from pymatgen.core import Molecule, Element
